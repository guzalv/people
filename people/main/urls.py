from django.urls import include, path
from django.views.generic import RedirectView

from . import views
from .models import Family, Food, Person

urlpatterns = [
    path("accounts/", include("django.contrib.auth.urls")),
    path(
        "",
        views.people_list,
        name="people-list",
    ),
    path(
        "family/add/",
        views.ProtectedCreateView.as_view(model=Family, fields="__all__"),
        name="create-family",
    ),
    path(
        "family/update/<int:pk>",
        views.ProtectedUpdateView.as_view(model=Family, fields="__all__"),
        name="update-family",
    ),
    path(
        "person/add/",
        views.ProtectedCreateView.as_view(model=Person, fields="__all__"),
        name="create-person",
    ),
    path(
        "person/update/<int:pk>",
        views.ProtectedUpdateView.as_view(model=Person, fields="__all__"),
        name="update-person",
    ),
    path(
        "food/add/",
        views.ProtectedCreateView.as_view(model=Food, fields="__all__"),
        name="create-food",
    ),
    path(
        "food/delete/<int:pk>",
        views.ProtectedDeleteView.as_view(model=Food, success_url="/food/list"),
        name="delete-food",
    ),
    path(
        "food/list/",
        views.ProtectedListView.as_view(model=Food),
        name="list-food",
    ),
    path(
        "food/update/<int:pk>",
        views.ProtectedUpdateView.as_view(model=Food, fields="__all__"),
        name="update-food",
    ),
]
