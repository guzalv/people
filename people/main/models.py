from django.db import models
from django.urls import reverse


class Family(models.Model):
    name = models.CharField(unique=True, blank=False, max_length=256)

    def get_absolute_url(self):
        return reverse("update-family", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name


class Food(models.Model):
    name = models.CharField(unique=True, blank=False, max_length=256)

    def get_absolute_url(self):
        return reverse("update-food", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name


class Person(models.Model):
    family = models.ForeignKey(
        "Family", on_delete=models.CASCADE, null=True, blank=True
    )
    name = models.CharField(blank=False, max_length=256)
    foods_liked = models.ManyToManyField(Food, blank=True, related_name="liked_by")
    foods_disliked = models.ManyToManyField(
        Food, blank=True, related_name="disliked_by"
    )

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("update-person", kwargs={"pk": self.pk})
