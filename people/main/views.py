from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.shortcuts import render
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views.generic import ListView
from django.views.generic.base import View
from django.views.generic.edit import CreateView, UpdateView, DeleteView

from .models import Family, Person


@login_required
def people_list(request):
    context = {
        "family_list": Family.objects.all(),
        "persons_without_family": Person.objects.filter(family=None),
    }
    return render(request, "main/people_list.html", context)


# Base class to inherit from when authentication is required
@method_decorator(login_required, name="dispatch")
class ProtectedView(View):
    pass


class ProtectedCreateView(ProtectedView, CreateView):
    pass


class ProtectedDeleteView(ProtectedView, DeleteView):
    pass


class ProtectedListView(ProtectedView, ListView):
    pass


class ProtectedUpdateView(ProtectedView, UpdateView):
    pass
