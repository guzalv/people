#!/usr/bin/env bash

set -euo pipefail

DIR=$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")
TAG=people
CODE_DIR=/people

sudo docker build --quiet --tag "${TAG}" "${DIR}"

run_opts=(
    --mount "type=bind,src=${DIR},dst=${CODE_DIR}" \
    --rm \
    -it \
    --user "$(id -u):$(id -g)" \
)

if grep -q runserver <<< "$@"; then
    run_opts+=(-p 8000:8000)
fi

sudo docker run \
    "${run_opts[@]}" \
    "${TAG}" sh -c "cd ${CODE_DIR} && $*"
